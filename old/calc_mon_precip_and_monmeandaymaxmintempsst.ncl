;*********************************************************************************************************************
;* calculates total precipitation and monthly daily temperature maximum and minimun for each month of the WRF output *
;*********************************************************************************************************************
begin

;  file_path = "/short/ku36/jtk561/WRF/WRFV3/run/run_WRF_ERA_SST/"; change input if necessary
;  file_path = "/short/ku36/mk5729/WRF/WRFV3/run/run_WRF_NOAA_SST_CONTROL_LA_NINA/"; change input if necessary
  file_path = "/short/ku36/mk5729/WRF/WRFV3/run/run_WRF_NOAA_SST_CONTROL/"; change input if necessary
  wrf_date = (/"2010-10", "2010-11", "2010-12", "2011-01", "2011-02", "2011-03", "2011-04", "2011-05"/)

; define lenght of each month (no leap year)
  days_per_month = (/31., 28., 31., 30., 31., 30., 31., 31., 30., 31., 30., 31./)
  days_per_month!0 = "month"
  days_per_month&month = fspan(1, 12, 12)  
  
; define output array (only one time necessary)  
  f = addfile(file_path +"wrfout_d01_" +wrf_date(0) +"-01_00:00:00", "r")
  
  t = f->T2

  dimsizes_t = dimsizes(t)
  t_dmax_avg = new((/12, dimsizes_t(1), dimsizes_t(2)/), typeof(t))
  t_dmax_avg!0 = "month"
  t_dmax_avg&month = fspan(1, 12, 12)

; define other variables  
  t_dmin_avg = t_dmax_avg
  total_rain = t_dmax_avg
  sst_d_avg = t_dmax_avg; daily SST average
 
  delete([/f, t/])
  
; loop over each month
  do j = 0, dimsizes(wrf_date) -1

    wrf_date_char = stringtochar(wrf_date(j))
    year = stringtofloat(charactertostring(wrf_date_char(0:3)))
    month = stringtofloat(charactertostring(wrf_date_char(5:6)))

    print("year: " +year +", month: " +month)

    f = addfile(file_path +"wrfout_d01_" +wrf_date(j) +"-01_00:00:00", "r")
  
; read first day to define month_avg array and read first precipitation
    t = f->T2
    t_dmax_avg_month = dim_max_n_Wrap(t, (/0/)) 
    t_dmin_avg_month = dim_min_n_Wrap(t, (/0/))

    sst = f->SST
    sst_d_avg_month = dim_avg_n_Wrap(sst, (/0/))
  
    rainc_start = f->RAINC(0, :, :)
    rainnc_start = f->RAINNC(0, :, :)
    rain_start = rainc_start +rainnc_start
  
    delete([/f, t, sst, rainc_start, rainnc_start/])
  
 ; loop over all days starting from second day
    do i = 2, days_per_month({month})

      if i .lt. 10 then
        day = "0" +tostring(i)
      else
        day = tostring(i)
      end if

      f = addfile(file_path +"wrfout_d01_" +wrf_date(j) +"-" +day +"_00:00:00", "r")
      t = f->T2
      t_dmax = dim_max_n_Wrap(t, (/0/))
      t_dmin = dim_min_n_Wrap(t, (/0/))
      t_dmax_avg_month = t_dmax_avg_month +t_dmax
      t_dmin_avg_month = t_dmin_avg_month +t_dmin

      sst = f->SST
      sst_d = dim_avg_n_Wrap(sst, (/0/))
      sst_d_avg_month = sst_d_avg_month +sst_d
      
; read precipitation from last timestep of the month
      if i .eq. days_per_month({month}) then
        rainc_end = f->RAINC(23, :, :)
        rainnc_end = f->RAINNC(23, :, :)
        rain_end = rainc_end +rainnc_end
      end if
  
      delete([/f, t, t_dmax, t_dmin, sst, sst_d/])
    end do

; write results to output array    
    t_dmax_avg({month}, :, :) = t_dmax_avg_month/days_per_month({month})
    t_dmin_avg({month}, :, :) = t_dmin_avg_month/days_per_month({month})

    sst_d_avg({month}, :, :) = sst_d_avg_month/days_per_month({month})
 
    total_rain({month}, :, :) = rain_end -rain_start

    delete([/t_dmax_avg_month, t_dmin_avg_month, sst_d_avg_month, rain_start, rain_end, month/])

  end do

; write to file
  path_out = "/short/ku36/mk5729/compare_era-i_noaa_wrf_output/"
;  fo = addfile(path_out +"tmax_tmin_sstmax_sstmin_rain_from_era-i_sst_input.nc", "c"); change name if necessary
;  fo = addfile(path_out +"tmax_tmin_sstmax_sstmin_rain_from_noaa_la-nina_sst_input.nc", "c"); change name if necessary
  fo = addfile(path_out +"tmax_tmin_sstmax_sstmin_rain_from_noaa_ctlr_sst_input.nc", "c"); change name if necessary

  fo->t_max = t_dmax_avg
  fo->t_min = t_dmin_avg

  fo->sst_avg = sst_d_avg
 
  fo->rain = total_rain

end
