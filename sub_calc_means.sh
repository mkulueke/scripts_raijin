#!/bin/sh
#PBS -q express
#PBS -P ku36
#PBS -l ncpus=64
#PBS -l mem=64GB
#PBS -l walltime=03:00:00
#PBS -j oe
#PBS -M Marco.Kulueke@murdoch.edu.au
#PBS -m ae

module load ncl

ncl /short/ku36/mk5729/scripts/calc_means.ncl 
